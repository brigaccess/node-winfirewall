#include <napi.h>
#include <locale>
#include <codecvt>	

#include "WinFirewallRule.h"

using namespace std;
using convert_type = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_type, wchar_t> converter;

// Converts wstring to utf string and returns napi utf8 string value
Napi::String ValueFromWString(Napi::Env env, wstring* str) {
	return Napi::String::New(env, converter.to_bytes(*str));
}

// Builds napi array of napi objects from security products ptrs vector
Napi::Array WinFirewallRulesVectorToArray(Napi::Env env, vector<WinFirewallRule*>* vector) {
	Napi::Array result = Napi::Array::New(env);

	if (vector == nullptr) {
		return result;
	}

	int index = 0;
	for (auto iter = vector->begin(); iter != vector->end(); iter++) {
		WinFirewallRule* rule = *iter;
		Napi::Object obj = Napi::Object::New(env);

		// Set object fields
		auto name = rule->getName();
		if (name != nullptr) {
			obj.Set("name", ValueFromWString(env, name));
		}
		auto description = rule->getDescription();
		if (description != nullptr) {
			obj.Set("description", ValueFromWString(env, description));
		}
		auto applicationName = rule->getApplicationName();
		if (applicationName != nullptr) {
			obj.Set("applicationName", ValueFromWString(env, applicationName));
		}
		auto serviceName = rule->getServiceName();
		if (serviceName != nullptr) {
			obj.Set("serviceName", ValueFromWString(env, serviceName));
		}
		auto protocol = rule->getProtocol();
		if (protocol != nullptr) {
			obj.Set("protocol", ValueFromWString(env, protocol));
		}
		auto localPorts = rule->getLocalPorts();
		if (localPorts != nullptr) {
			obj.Set("localPorts", ValueFromWString(env, localPorts));
		}
		auto localAddresses = rule->getLocalAddresses();
		if (localAddresses != nullptr) {
			obj.Set("localAddresses", ValueFromWString(env, localAddresses));
		}
		auto remotePorts = rule->getRemotePorts();
		if (remotePorts != nullptr) {
			obj.Set("remotePorts", ValueFromWString(env, remotePorts));
		}
		auto remoteAddresses = rule->getRemoteAddresses();
		if (remoteAddresses != nullptr) {
			obj.Set("remoteAddresses", ValueFromWString(env, remoteAddresses));
		}
		auto IMCPTypecode = rule->getIMCPTypecode();
		if (IMCPTypecode != nullptr) {
			obj.Set("IMCPTypecode", ValueFromWString(env, IMCPTypecode));
		}
		auto direction = rule->getDirection();
		if (direction != nullptr) {
			obj.Set("direction", ValueFromWString(env, direction));
		}
		auto action = rule->getAction();
		if (action != nullptr) {
			obj.Set("action", ValueFromWString(env, action));
		}
		obj.Set("profiles", rule->getProfiles());

		auto enabled = rule->getEnabled();
		if (enabled) {
			obj.Set("enabled", enabled);
		}
		auto edgeTraversal = rule->getEdgeTraversal();
		if (edgeTraversal) {
			obj.Set("edgeTraversal", edgeTraversal);
		}

		// Add object to array
		result.Set(index++, obj);
	}
	return result;
}

Napi::Object Method(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	auto rules = GetFirewallRules();
	return WinFirewallRulesVectorToArray(env, rules);
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	// Initialize COM
	HRESULT result = CoInitializeEx(0, COINIT_APARTMENTTHREADED);
	if (result != S_OK) {
		// TODO Return "Initialization failed" somehow
	}

	exports.Set(
		Napi::String::New(env, "getFirewallRules"),
		Napi::Function::New(env, Method)
	);
	return exports;
}

NODE_API_MODULE(hello, Init)