#pragma once
#include <string>
#include <vector>
#include <windows.h>
#include <comutil.h>
#include <atlcomcli.h>
#include <netfw.h>
#include <assert.h>

#pragma comment( lib, "ole32.lib" )
#pragma comment( lib, "oleaut32.lib" )

#define PROTO_TCP L"TCP"
#define PROTO_UDP L"UDP"
#define RULE_IN L"In"
#define RULE_OUT L"Out"
#define ACTION_BLOCK L"Block"
#define ACTION_ALLOW L"Allow"
#define RULE_ENABLE L"TRUE"
#define RULE_DISABLE L"FALSE"

using namespace std;

class WinFirewallRule {
protected:
	wstring* name = nullptr;
	wstring* description = nullptr;
	wstring* applicationName = nullptr;
	wstring* serviceName = nullptr;
	wstring* protocol = nullptr;
	wstring* localPorts = nullptr;
	wstring* localAddresses = nullptr;
	wstring* remotePorts = nullptr;
	wstring* remoteAddresses = nullptr;
	wstring* imcpTypecode = nullptr;
	int profiles = 0;
	wstring* direction = nullptr;
	wstring* action = nullptr;
	bool enabled = false;
	bool edgeTraversal = false;

	WinFirewallRule() {}
public:
	/*
	 * Builds WinFirewallRule from INetFwRule if possible. Otherwise, returns NULL.
	 * Note that this method does not release product so you have to do it on the caller side.
	 */
	static WinFirewallRule* fromINetFwRule(INetFwRule* rule);
	wstring* const getName();
	wstring* const getDescription();
	wstring* const getApplicationName();
	wstring* const getServiceName();
	wstring* const getProtocol();
	wstring* const getLocalPorts();
	wstring* const getLocalAddresses();
	wstring* const getRemotePorts();
	wstring* const getRemoteAddresses();
	wstring* const getIMCPTypecode();
	wstring* const getDirection();
	wstring* const getAction();
	int getProfiles();
	bool getEnabled();
	bool getEdgeTraversal();
};

vector<WinFirewallRule*>* GetFirewallRules();