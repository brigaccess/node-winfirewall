#include "WinFirewallRule.h"

vector<WinFirewallRule*>* GetFirewallRules() {
	INetFwPolicy2* netPolicy;
	HRESULT result = CoCreateInstance(
		__uuidof(NetFwPolicy2), NULL,
		CLSCTX_INPROC_SERVER, __uuidof(INetFwPolicy2),
		(void**)&netPolicy
	);

	if (result != S_OK) {
		return nullptr;
	}

	INetFwRules* rules;
	result = netPolicy->get_Rules(&rules);
	if (result != S_OK) {
		return nullptr;
	}

	long ruleCount;
	result = rules->get_Count(&ruleCount);
	if (result != S_OK) {
		return nullptr;
	}

	IUnknown* enumerator;
	IEnumVARIANT* variant;
	rules->get__NewEnum(&enumerator);

	if (enumerator)
	{
		result = enumerator->QueryInterface(__uuidof(IEnumVARIANT), (void**)& variant);
	}
	CComVariant cvar;
	ULONG fetched;
	auto rulevector = new vector<WinFirewallRule*>();
	while (result == S_OK && result != S_FALSE) {
		cvar.Clear();
		result = variant->Next(1, &cvar, &fetched);

		if (result != S_FALSE) {
			result = cvar.ChangeType(VT_DISPATCH);
			INetFwRule2* rule;
			if (result == S_OK) {
				result = (V_DISPATCH(&cvar))->QueryInterface(
					__uuidof(INetFwRule), reinterpret_cast<void**>(&rule)
				);
				if (result == S_OK) {
					rulevector->push_back(WinFirewallRule::fromINetFwRule(rule));
				}
			}
		}
	}
	return rulevector;
}

WinFirewallRule* WinFirewallRule::fromINetFwRule(INetFwRule* rule)
{
	WinFirewallRule* instance = new WinFirewallRule();
	BSTR strbuf;
	if (rule->get_Name(&strbuf) == S_OK && strbuf != nullptr) {
		instance->name = new wstring(strbuf);
		SysFreeString(strbuf);
	}
	if (rule->get_Description(&strbuf) == S_OK && strbuf != nullptr) {
		instance->description = new wstring(strbuf);
		SysFreeString(strbuf);
	}
	if (rule->get_ApplicationName(&strbuf) == S_OK && strbuf != nullptr) {
		instance->applicationName = new wstring(strbuf);
		SysFreeString(strbuf);
	}
	if (rule->get_ServiceName(&strbuf) == S_OK && strbuf != nullptr) {
		instance->serviceName = new wstring(strbuf);
		SysFreeString(strbuf);
	}
	long proto = 0;
	if (rule->get_Protocol(&proto) == S_OK) {
		if (proto == NET_FW_IP_PROTOCOL_TCP) {
			instance->protocol = new wstring(PROTO_TCP);
		}
		else if (proto == NET_FW_IP_PROTOCOL_UDP) {
			instance->protocol = new wstring(PROTO_UDP);
		}

		if (proto != NET_FW_IP_VERSION_V4 && proto != NET_FW_IP_VERSION_V6) {
			if (rule->get_LocalPorts(&strbuf) == S_OK && strbuf != nullptr) {
				instance->localPorts = new wstring(strbuf);
				SysFreeString(strbuf);
			}

			if (rule->get_RemotePorts(&strbuf) == S_OK && strbuf != nullptr) {
				instance->remotePorts = new wstring(strbuf);
				SysFreeString(strbuf);
			}
		}
		else {
			if (rule->get_IcmpTypesAndCodes(&strbuf) == S_OK && strbuf != nullptr) {
				instance->imcpTypecode = new wstring(strbuf);
				SysFreeString(strbuf);
			}
		}
	}

	if (rule->get_LocalAddresses(&strbuf) == S_OK && strbuf != nullptr) {
		instance->localAddresses = new wstring(strbuf);
		SysFreeString(strbuf);
	}

	if (rule->get_RemoteAddresses(&strbuf) == S_OK && strbuf != nullptr) {
		instance->remoteAddresses = new wstring(strbuf);
		SysFreeString(strbuf);
	}

	long profiles;
	if (rule->get_Profiles(&profiles) == S_OK) {
		instance->profiles = profiles;
	}

	NET_FW_RULE_DIRECTION direction;
	if (rule->get_Direction(&direction) == S_OK) {
		if (direction == NET_FW_RULE_DIR_IN) {
			instance->direction = new wstring(RULE_IN);
		}
		else if (direction == NET_FW_RULE_DIR_OUT) {
			instance->direction = new wstring(RULE_OUT);
		}
	}

	NET_FW_ACTION action;
	if (rule->get_Action(&action) == S_OK) {
		if (action == NET_FW_ACTION_BLOCK) {
			instance->action = new wstring(ACTION_BLOCK);
		}
		else if (action == NET_FW_ACTION_ALLOW) {
			instance->action = new wstring(ACTION_ALLOW);
		}
	}

	VARIANT_BOOL enabled;
	if (rule->get_Enabled(&enabled) == S_OK) {
		instance->enabled = enabled ? true : false;
	}
	return instance;
}

wstring* const WinFirewallRule::getName() {
	return this->name;
}

wstring* const WinFirewallRule::getDescription() {
	return this->description;
}

wstring* const WinFirewallRule::getApplicationName() {
	return this->applicationName;
}

wstring* const WinFirewallRule::getServiceName() {
	return this->serviceName;
}

wstring* const WinFirewallRule::getProtocol() {
	return this->protocol;
}

wstring* const WinFirewallRule::getLocalPorts() {
	return this->localPorts;
}

wstring* const WinFirewallRule::getLocalAddresses() {
	return this->localAddresses;
}

wstring* const WinFirewallRule::getRemotePorts() {
	return this->remotePorts;
}

wstring* const WinFirewallRule::getRemoteAddresses() {
	return this->remoteAddresses;
}

wstring* const WinFirewallRule::getIMCPTypecode() {
	return this->imcpTypecode;
}

wstring* const WinFirewallRule::getDirection() {
	return this->direction;
}

wstring* const WinFirewallRule::getAction() {
	return this->action;
}

int WinFirewallRule::getProfiles() {
	return this->profiles;
}

bool WinFirewallRule::getEnabled() {
	return this->enabled;
}

bool WinFirewallRule::getEdgeTraversal() {
	return this->edgeTraversal;
}
